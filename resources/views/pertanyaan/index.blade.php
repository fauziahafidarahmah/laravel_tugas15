@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if('session('success'))
                <div class="alert alert-success">
                  {{ session('success')}}
                </div>
              @endif
              <a class="btn btn-primary mb-2" href="{{route('pertanyaan.create')}}">Membuat Pertanyaan Baru</button>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $pertanyaan)
                        <tr>
                            <td> {{$key+1}}</td>
                            <td> {{$pertanyaan ->$title}}</td>
                            <td> {{$pertanyaan ->$body}}</td>
                            <td style="display: flex;">
                              <a href="{{route('pertanyaan.show', ['id'=> $pertanyaan->id)}}" class="btn btn-info btn-sm">show</a>
                              <a href="{{route('pertanyaan.edit', ['id'=> $pertanyaan->id)}}" class="btn btn-default btn-sm ">edit</a>
                              <form action="{{route('pertanyaan.destroy', ['id'=> $pertanyaan->id)}}" method="post">
                              @csrf
                              @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                              </form>
                            </td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

            </div>
    </div>
@endsection