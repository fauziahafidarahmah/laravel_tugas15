<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }
    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'title' => 'required|unique:posts',
            'body' => 'required                                                                                                                                                                                                                                                      '
        ]);
        //$query = DB::table('pertanyaan')->insert(
        //    ["title"=> $request["title"],
        //    "body"=> $request["body"]]
        //);

        //$pertanyaan = new Pertanyaan;
        //$pertanyaan->title = $request["title"];
        //$pertanyaan->body = $request["body"];
        //$pertanyaan->save(); // insert into posts (title, body) values

        $pertanyaan = Pertanyaan::create([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        
        return redirect('/pertanyaan')->with('session', 'Pertanyaan Berhasil Disimpan');
    }
    
    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        dd($pertanyaan);
        $pertanyaan = POST::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    
    }
    public function show($id) {
        //$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = Post::find($id);
        //dd($pertanyaan);
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id) {
        //$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = Post::find($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){

        //$query= DB::table('pertanyaan')->where('id', $id)->update(['title'=> $request['title'], 'body' => $request['body']]);
        $update = Post::where('id', $id)->update([
            "title"=>$request["title"],
            "body"=>$request["body"]
        ]);
        
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }    
    
    public function destroy($id) {
        //$query = DB::table('pertanyaan')->where('id', $id)->delete();
        POST::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
